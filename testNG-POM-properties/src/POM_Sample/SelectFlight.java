package POM_Sample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SelectFlight {
	
	private static List<WebElement> elements;
	
	private static WebElement element = null;
	private static Properties prop = null;
	private static FileInputStream in = null;
	static{
		try {
		
			prop = new Properties();
			in = new FileInputStream("locators.properties");
			prop.load(in);
			in.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static List<WebElement> outFlight(WebDriver driver){
		elements = driver.findElements(By.xpath(prop.getProperty("outFlight")));
		return elements;
	}
	public static List<WebElement> inFlight(WebDriver driver){
		elements = driver.findElements(By.xpath(prop.getProperty("inFlight")));
		return elements;
	}
	public static WebElement reserveFlights(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("reserveFlights")));
		return element;
	}
	
}
