package POM_Sample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BookFlightPage {
	private static WebElement element = null;
	private static Properties prop = null;
	private static FileInputStream in = null;
	static{
		try {
		
			prop = new Properties();
			in = new FileInputStream("locators.properties");
			prop.load(in);
			in.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static WebElement firstName(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("firstName")));
		return element;
	}

	public static WebElement lastName(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("lastName")));
		return element;
	}
	
	public static WebElement creditnumber(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("creditnumber")));
		return element;
	}
	

	public static WebElement buyFlights(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("buyFlights")));
		return element;
	}
}
