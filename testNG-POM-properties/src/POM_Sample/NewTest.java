package POM_Sample;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class NewTest {
	private static WebDriver driver = null;
	
  @Test(priority=1)
  public void login() {
	  LoginPage.user(driver).sendKeys("kevinnguye86");
		LoginPage.password(driver).sendKeys("Hongluong67");
		LoginPage.login(driver).click();
  }
  
  @Test(priority=2)
  public void flightFinder() {
	  FlightFinder.oneWay(driver).click();
	  FlightFinder.continueButton(driver).click();
  }
  
  @Test(priority=3)
  public void selectFlight() {
	  
		
		List<WebElement> out = SelectFlight.outFlight(driver);
		List<WebElement> in = SelectFlight.inFlight(driver);
		
		
		for(int i = 0; i < out.size(); i++){
			out.get(i).click();
			in.get(i).click();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		SelectFlight.reserveFlights(driver).click();
  }
  
  @Test(priority=4)
  public void bookFlight() {
	  BookFlightPage.firstName(driver).sendKeys("Kevin");
	  BookFlightPage.lastName(driver).sendKeys("Nguyen");
	  BookFlightPage.creditnumber(driver).sendKeys("1234567890123456");
	  BookFlightPage.buyFlights(driver).click();
  }
  
  @BeforeMethod
  public void beforeMethod() {
  }

  @AfterMethod
  public void afterMethod() {
  }


  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },
    };
  }
  @BeforeClass
  public void beforeClass() {
	  File file = new File("C:/Jar_folder/selenium-2.53.1/chromedriver.exe");
	  System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
	  driver = new ChromeDriver();
	  driver.get("http://newtours.demoaut.com/mercurywelcome.php");
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
