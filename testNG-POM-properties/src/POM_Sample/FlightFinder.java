package POM_Sample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FlightFinder {
	
	private static WebElement element = null;
	private static Properties prop = null;
	private static FileInputStream in = null;
	static{
		try {
		
			prop = new Properties();
			in = new FileInputStream("locators.properties");
			prop.load(in);
			in.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static WebElement oneWay(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("oneWay")));
		return element;
	}
	
	public static WebElement roundTrip(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("roundTrip")));
		return element;
	}
	
	public static WebElement continueButton(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("findFlights")));
		return element;
	}
}
