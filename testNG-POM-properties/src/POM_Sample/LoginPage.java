package POM_Sample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	private static WebElement element = null;
	private static Properties prop = null;
	private static FileInputStream in = null;
	static{
		try {
		
			prop = new Properties();
			in = new FileInputStream("locators.properties");
			prop.load(in);
			in.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static WebElement user(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("username")));
		return element;
	}

	public static WebElement password(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("password")));
		return element;
	}
	
	public static WebElement login(WebDriver driver){
		element = driver.findElement(By.xpath(prop.getProperty("login")));
		return element;
	}
}
