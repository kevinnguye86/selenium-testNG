package properties_xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class Properties_to_xml {

	public static void main(String[] args) {
		Properties prop = new Properties();
		FileInputStream in;
    	OutputStream os;
    	
		try {
			in = new FileInputStream("locators.properties");
			prop.load(in);
			in.close();
			
			os = new FileOutputStream("C:/Eclipse/page_elements.xml");
			
			//store the properties detail into a pre-defined XML file
			prop.storeToXML(os, "Page Elements","UTF-8");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	
    	
    	
    	System.out.println("Done");
	}

}
