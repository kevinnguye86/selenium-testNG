package JAXB_Sample;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ConvertXML2Object {

	public static void main(String[] args) {
		try{
			
			File file = new File("C:\\Eclipse\\newXML146904190678.xml");
			
			JAXBContext jcont = JAXBContext.newInstance(EmployeeInfo.class);
			
			Unmarshaller jaxbunm = jcont.createUnmarshaller();
			EmployeeInfo emp = (EmployeeInfo) jaxbunm.unmarshal(file);
			System.out.println(emp.toString());
			
		}
		catch(JAXBException e){
			e.printStackTrace();
		}
		
	}

}
