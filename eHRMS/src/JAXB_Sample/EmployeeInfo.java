package JAXB_Sample;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EmployeeInfo {
	int employeeId;
	String employeeName;
	double salary;
	int age;
	
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString(){
		return this.employeeId + "--" + this.employeeName + "--" + this.age + "--" + this.salary;
		
	}
	
}
