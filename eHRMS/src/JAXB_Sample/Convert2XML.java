package JAXB_Sample;

import java.io.File;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Convert2XML {

	public static void main(String[] args) {
		
		EmployeeInfo emp = new EmployeeInfo();
		
		for(int i = 0; i < 5; i++){
		emp.setAge(29+i);
		emp.setEmployeeName("Kevin");
		emp.setEmployeeId(1+i);
		emp.setSalary(1400+i);
		
		Date d = new Date();
		File file = new File("C:\\Eclipse\\newXML.xml");
		try{
			JAXBContext jcont = JAXBContext.newInstance(EmployeeInfo.class);
			
			Marshaller jaxbm = jcont.createMarshaller();
			
			jaxbm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbm.marshal(emp, file);
			jaxbm.marshal(emp, System.out);
		
			Thread.sleep(500);
		}
		catch(JAXBException e){
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		}
	}

}
