package eHRMS;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import org.testng.Assert;


public class NewTest {
	WebDriver chr;
	
	@Test(priority = 1, dataProvider = "dp1")
	public void login(String username, String password) {
		
	  
	  chr.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[1]/td[2]/input")).clear();
	  
	  chr.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[1]/td[2]/input")).sendKeys(username);
	  chr.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/input")).sendKeys(password);
	  chr.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[5]/td[2]/input[1]")).click();
	  
	  
  }
  
  @Test(priority = 2)
  public void openEmployeeManagement(){
	  
	  String oldurl = chr.getCurrentUrl();
	  
	  chr.findElement(By.xpath("//*[@id='accordion']/div[2]/table/tbody/tr/td[2]/a")).click();
	  
	  //Assert.assertEquals(chr.getCurrentUrl(), oldurl);
	  
  }
  
  @Test(priority = 3)
  public void addNewLeave(){
	  //Click add new leave link
	  chr.findElement(By.xpath("//*[@id='dt_example']/table/tbody/tr[2]/td[2]/table/tbody/tr/td/form/table/tbody/tr[3]/td/p/a/font")).click();
	  
	  //Select type of leave
	  Select select = new Select(chr.findElement(By.xpath("//*[@id='leaveType']")));
	  select.selectByIndex(1);
	  
	  
	  //From date
	  chr.findElement(By.xpath("//*[@id='fromDate']")).sendKeys("07/21/2016");
	  
	  //To date
	  chr.findElement(By.xpath("//*[@id='toDate']")).sendKeys("07/29/2016");
	  
	  //Reason
	  chr.findElement(By.xpath("//*[@id='reason']")).sendKeys("Tired of working");
	  
	  //Submit
	  chr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[8]/td[2]/input")).click();
	  /*
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	  
  }
  
  @Test(priority = 4)
  public void openEmployeeTimeSheet(){
	  chr.findElement(By.xpath("//*[@id='accordion']/div[3]/table/tbody/tr/td[2]/a")).click();
	  
  }
  
  @Test(priority = 5)
  public void addTimeSheet(){
	  //click add time sheet
	  chr.findElement(By.xpath("//*[@id='dt_example']/table/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td/p/a/font")).click();
	  
	  
	//Select type of leave
	  Select select = new Select(chr.findElement(By.xpath("//*[@id='empShift']")));
	  select.selectByIndex(1);
	  
	  //add date
	  chr.findElement(By.xpath("//*[@id='date']")).sendKeys("07/25/2016");
	  
	  //add hour
	  chr.findElement(By.xpath("//*[@id='hours']")).sendKeys("45");
	  
	  //add job done
	  chr.findElement(By.xpath("//*[@id='jobDone']")).sendKeys("Completed 3 tests");
	  
	  //add remarks
	  chr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[8]/td[2]/textarea")).sendKeys("optional");
	  
	  //Click Save
	  chr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[10]/td[2]/input")).click();
  }
  
  @Test(priority = 6)
  public void logout(){
	  chr.findElement(By.xpath("/html/body/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td[3]/font/a/img")).click();
	  
  }
  
  @BeforeMethod
  public void beforeMethod() {
  }

  @AfterMethod
  public void afterMethod() {
  }


  @DataProvider
  public Object[][] dp1() {
    return new Object[][] {
      new Object[] { "guest", "off2hand" },
    };
  }
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }
  
  @BeforeClass
  public void beforeClass() {
	  File file = new File("C:/Jar_folder/selenium-2.53.1/chromedriver.exe");
	  System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
	  chr = new ChromeDriver();
	  chr.get("http://192.168.60.11:7001/eHRMS/login.jsp");
  }

  @AfterClass
  public void afterClass() {
	  chr.quit();
  }

}
